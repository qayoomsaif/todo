import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  Keyboard,
  TextInput,
} from 'react-native';
import {LoginController, styles} from './index';
import {LoadingBar, Input} from 'src/components/';
import {connect} from 'react-redux';
import {changeUserInfo} from 'redux-store/actions';

class LoginComponent extends Component {
  controller = new LoginController(this);
  state = {
    email: null,
    loading: false,
    password: null,
  };

  handleBackButton() {
    this.controller.goToBack();
    return true;
  }
  setUserInfoInRedux(respose) {
    this.props.dispatch(changeUserInfo(respose));
  }
  onLoading(value) {
    this.setState({loading: value || false});
  }

  render() {
    const {email, password, loading} = this.state;
    return (
      <View style={styles.maincontainer}>
        <View style={styles.logoimagecontainer}>
          <Image
            style={styles.logoimage}
            source={require('src/assets/icon/logo.png')}
          />
        </View>
        <Text style={styles.headingtext}> Login </Text>

        <TextInput
          style={styles.inputemailtext}
          placeholder={'Enter your email'}
          onChangeText={(text) => this.setState({email: text.toLowerCase()})}
          value={email}
          keyboardType={'email-address'}
          maxLength={100}
          returnKeyType={'next'}
          onSubmitEditing={() => this.passwordInput.focus()}
          ref={(ref) => {
            this.emailInput = ref;
          }}
        />

        <TextInput
          style={styles.inputemailtext}
          placeholder={'Enter your Password'}
          onChangeText={(text) => this.setState({password: text.toLowerCase()})}
          value={password}
          secureTextEntry={true}
          returnKeyType={'done'}
          maxLength={100}
          onSubmitEditing={() => {
            Keyboard.dismiss();
            this.controller.signInWithEmail(email, password);
          }}
          ref={(ref) => {
            this.passwordInput = ref;
          }}
        />

        <TouchableOpacity
          style={styles.continuebuttoncontainer}
          onPress={() => {
            Keyboard.dismiss();
            this.controller.signInWithEmail(email, password);
          }}>
          <Text style={styles.continuebuttontext}>Login</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.continuebuttoncontainer}
          onPress={() => this.controller.goToSignupComponent()}>
          <Text style={styles.socialloginbuttontext}>Signup</Text>
        </TouchableOpacity>

        <View style={styles.ormaincontainer}>
          <Text style={styles.ortext}>{'  '}OR</Text>
        </View>

        <TouchableOpacity
          style={styles.socialloginbuttonfacebook}
          onPress={this.controller.loginWithFacebook}>
          <View style={styles.socialloginbuttonimagecontainer}>
            <Image
              style={styles.socialloginbuttonimage}
              source={require('src/assets/icon/facebookLogo.png')}
            />
          </View>
          <Text style={styles.socialloginbuttontext}>
            {'Login With Facebook'}
          </Text>
        </TouchableOpacity>
        {loading ? (
          <View style={styles.loadingcontainer}>
            <LoadingBar />
          </View>
        ) : null}
      </View>
    );
  }
}
const mapStateToProps = ({account}) => {
  const {userInfo} = account;
  return {userInfo};
};
export default connect(mapStateToProps)(LoginComponent);
