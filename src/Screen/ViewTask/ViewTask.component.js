import React, {Component} from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  ActivityIndicator,
  Image,
  BackHandler,
  FlatList,
} from 'react-native';
import styles from './ViewTask.style';
import ViewTaskController from './ViewTask.controller';
import {Header, LoadingBar, Input} from 'components';
import CardView from 'react-native-cardview';

import {connect} from 'react-redux';
import {changeUserInfo, changeToDoList, getTodoList} from 'redux-store/actions';

class ViewTaskComponent extends Component {
  constructor(props) {
    super(props);
    this.controller = new ViewTaskController(this);
    this.state = {
      title: null,
      toDoListData: [
        {
          title: 'task adjkhaskjdhs asdkbsjkd sdjss askj ddhs',
          des: 'i want to complet my tasti want to complet my tasti want to complet my tasti want to complet my tasti want to complet my tasti want to complet my tasti want to complet my tasti want to complet my tasti want to complet my tasti want to complet my tasti want to complet my tasti want to complet my tast',
          taskId: '1',
        },
        {title: 'assigemt', des: 'i want to complet my assigemt', taskId: '2'},
      ],
    };
  }
  upDateToDoList(updateList) {
    this.props.dispatch(changeToDoList(updateList));
  }
  componentDidMount() {
    this.props.dispatch(
      getTodoList('to_do_list' + '/' + this.props.userInfo.id),
    );
    // BackHandler.addEventListener('ViewTaskComponent', this.handleBackButton.bind(this))
  }
  setLoading(value) {
    this.setState({loading: value ? value : false});
  }
  componentWillUnmount() {
    // BackHandler.removeEventListener('ViewTaskComponent', this.handleBackButton.bind(this))
  }
  _renderRow(item) {
    return (
      <CardView
        cardElevation={3}
        cardMaxElevation={3}
        cornerRadius={styles.listItemMainContainer.borderRadius}
        style={styles.listItemMainContainer}>
        <View style={styles.listItemSubContainer}>
          <Text style={styles.listItemTitleText}>{item.title}</Text>
          <Text style={styles.listItemDesText}>{item.des}</Text>
        </View>
        <Text style={styles.dateText}>{item.date}</Text>
        <View style={styles.listItemButtonMainContainer}>
          <TouchableOpacity
            style={styles.listItemButtonContainer}
            onPress={() =>
              this.controller.delete(
                item,
                this.props.userInfo,
                this.props.toDoList,
              )
            }>
            <Image
              style={styles.listItemButtonIcon}
              source={require('src/assets/icon/delete.png')}
              resizeMode={'contain'}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.listItemButtonContainer}
            onPress={() => this.controller.goToEdit(item)}>
            <Image
              style={styles.listItemButtonIcon}
              source={require('src/assets/icon/edit.png')}
              resizeMode={'contain'}
            />
          </TouchableOpacity>
        </View>
      </CardView>
    );
  }
  handleBackButton() {
    this.controller.goToBack();
  }
  setError(value) {
    this.setState({errType: value ? value : 0});
  }
  render() {
    const {loading} = this.state;
    const {toDoList} = this.props;
    return (
      <View style={styles.mainContainer}>
        <Header
          title={'Task'}
          onPressBack={() => this.handleBackButton()}
          backImage={require('src/assets/icon/back.png')}
        />
        <CardView
          cardElevation={3}
          cardMaxElevation={3}
          cornerRadius={styles.addMainContainer.borderRadius}
          style={styles.addMainContainer}>
          <TouchableOpacity
            style={styles.addSubContainer}
            onPress={() => this.controller.goToAdd()}>
            <Text style={styles.addText}>{'+'}</Text>
          </TouchableOpacity>
        </CardView>

        {toDoList.length ? (
          <FlatList
            showsVerticalScrollIndicator={false}
            data={toDoList}
            renderItem={(item) => this._renderRow(item.item, item.index)}
            keyExtractor={(item) => null}
          />
        ) : null}

        {loading ? <LoadingBar style={styles.loading} /> : null}
      </View>
    );
  }
}
const mapStateToProps = ({account}) => {
  const {userInfo, toDoList} = account;
  return {userInfo, toDoList};
};

export default connect(mapStateToProps)(ViewTaskComponent);
