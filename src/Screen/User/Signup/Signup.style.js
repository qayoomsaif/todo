import {
  font,
  resize,
  color,
  heightScale,
  widthScaleSub,
  widthScale,
  width,
  height,
} from 'globalStyle';

import {StyleSheet} from 'react-native';

export default styles = StyleSheet.create({
  maincontainer: {
    flex: 1,
    alignItems: 'center',
    // justifyContent: 'center',
    backgroundColor: color.bgDark,
  },
  header: {
    width: width,
    alignItems: 'flex-end',
    alignSelf: 'center',
  },
  crossbutton: {
    marginRight: widthScale(32),
    marginTop: heightScale(20),
    height: heightScale(16),
    width: heightScale(16),
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  crossbuttonimage: {
    height: heightScale(16),
    width: heightScale(16),
  },
  logoimagecontainer: {
    marginTop: heightScale(20),
    alignSelf: 'center',
    height: heightScale(100),
    width: heightScale(100),
  },
  logoimage: {
    height: undefined,
    width: undefined,
    flex: 1,
  },
  headingtext: {
    marginVertical: heightScale(25),
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    fontFamily: font.two.semiBold,
    color: color.black,
    fontSize: heightScale(24),
  },

  inputemailtext: {
    width: widthScaleSub(40),
    paddingHorizontal: widthScale(10),
    height: heightScale(50),
    backgroundColor: color.bgLight,
    marginVertical: heightScale(5),
    borderRadius: widthScale(4),
    fontFamily: font.two.regular,
    color: color.textDark,
    fontSize: heightScale(16),
  },

  continuebuttoncontainer: {
    width: widthScaleSub(40),
    marginTop: heightScale(16),
    height: heightScale(50),
    borderRadius: widthScale(4),
    backgroundColor: color.app,
    justifyContent: 'center',
    alignItems: 'center',
  },
  continuebuttontext: {
    fontFamily: font.two.semiBold,
    color: color.white,
    fontSize: heightScale(21),
  },
  loadingcontainer: {
    width: width,
    height: height,
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F5FCFF88',
  },
});
