import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import styles from './header.style'
import CardView from 'react-native-cardview'


export default class Header extends Component {
    render() {
        const { back, title, onPressBack, onPressOption1, onPressOption2,
            backImage, optionImage1, optionImage2, multipleOption, optiontitle1
        } = this.props
        return (
            !multipleOption ? <CardView
                cardElevation={3} cardMaxElevation={3} cornerRadius={0}
                style={styles.maincontainer }

            >
                <View style={styles.submaincontiner}>
                    <View style={styles.backmaincontiner}>
                        <TouchableOpacity
                            style={styles.backcontiner}
                            onPress={() => onPressBack ? onPressBack() : null}
                        >
                            <Image
                                style={styles.backimage}
                                source={backImage}
                                resizeMode={'contain'}
                            />
                        </TouchableOpacity>
                    </View>
                    <View>
                    <Text style= {styles.titleText}>
                        {title}
                    </Text>
                    </View>
                    <View style={styles.titlecontiner}>
                        <Image
                            style={styles.logoimage}
                            source={require('src/assets/icon/youcan_white_logo.png')}
                            resizeMode='contain'
                        />
                    </View>
                </View>
            </CardView> :
                <CardView
                    cardElevation={1} cardMaxElevation={1} cornerRadius={0}
                    style={styles.maincontainer }
                >
                    <View style={styles.submaincontiner}>
                        <View style={styles.backmaincontiner}>
                            <View
                                style={styles.backcontiner}
                            >

                                <Image
                                    style={styles.logoimage}
                                    source={require('src/assets/icon/youcan_white_logo.png')}
                                    resizeMode='contain'
                                />
                            </View>
                        </View>
                        <TouchableOpacity style={styles.titlecontiner}
                            onPress={() => onPressBack ? onPressBack() : null}
                        >
                            <Image
                                style={styles.backimage}
                                source={backImage}
                                resizeMode={'contain'}
                            />
                        </TouchableOpacity>
                    </View>

                </CardView>
        );
    }
}
module.exports = { Header }
