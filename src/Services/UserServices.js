export default class UserServices {
  constructor(service) {
    this.service = service;
  }
  async signup(phoneNumber, country) {
    let url = 'api/v1/user/phone-signup';
    let para = {phoneNumber: phoneNumber, country: country};
    let response = await this.service.post(url, para, false);
    return response.data ? response.data : response;
  }
  async signin(phoneNumber, country) {
    let url = 'api/v1/user/phone-signin';
    let para = {phoneNumber: phoneNumber};
    let response = await this.service.post(url, para, false);
    return response.data ? response.data : response;
  }
}
