import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import styles from './tryAgain.style'
export default class TryAgain extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { title, msg, show, top, bottom, style, position } = this.props
        return (
            show ? <View style={[style ? style : styles.maincontainer, 
        ]
            }>
                <Image
                    style={styles.clodimage}
                    source={require('src/assets/icon/cloud.png')}
                    resizeMode={'contain'}
                />
                <Text style={styles.msgtitletext}>
                    {title || 'Oops!'}
                </Text>
                <Text style={styles.msgtext}>
                    {msg || 'Something went to wrong please try again later'}
                </Text>
                <TouchableOpacity
                    onPress={() => this.props.onPress ? this.props.onPress() : null}
                    style={styles.tryagaincontainer}
                >
                    <Text style={styles.retrytext}>
                        Try again
                    </Text>
                </TouchableOpacity>
            </View> :
                null

        );
    }
}
module.exports = { TryAgain }
