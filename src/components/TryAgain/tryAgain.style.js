import {StyleSheet} from 'react-native';

import {
  font,
  resize,
  color,
  heightScale,
  widthScaleSub,
  widthScale,
  width,
  height,
  square,
} from 'globalStyle';

export default styles = StyleSheet.create({
  maincontainer: {
    justifyContent: 'center',
    alignItems: 'center',

    flex: 1,
  },
  clodimage: {
    width: widthScale(180),
    height: heightScale(100),
  },
  msgtitletext: {
    textAlign: 'center',
    fontFamily: font.one.semiBold,
    fontSize: widthScale(20),

    marginTop: heightScale(40),
    paddingLeft: widthScale(40),
    paddingRight: widthScale(40),
  },
  msgtext: {
    textAlign: 'center',
    fontFamily: font.one.regular,
    color: '#cccccc',
    marginTop: heightScale(12),
    fontSize: widthScale(16),
    paddingLeft: widthScale(67),
    paddingRight: widthScale(67),
  },
  tryagaincontainer: {
    marginTop: heightScale(40),
    width: widthScale(140),
    height: heightScale(40),
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: heightScale(6),
    backgroundColor: color.app,
  },
  retrytext: {
    fontFamily: font.one.semiBold,
    color: color.white,
    fontSize: widthScale(21),
    paddingBottom: heightScale(3),
  },
});
