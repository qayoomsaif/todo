import React from "react";
import { View, Text, Image, ImageBackground, TouchableOpacity } from 'react-native';

class ErrorBoundary extends React.Component {
    constructor(props) {
      super(props);
      this.state = { hasError: false };
    }
  
    static getDerivedStateFromError(error) {
      // console.log('We did catch', error);
      return { hasError: true };
    }
  
    render() {
      if (this.state.hasError) {
        return <Text>Something went wrong.</Text>;
      }
  
      return this.props.children;
    }
  }
module.exports = { ErrorBoundary }