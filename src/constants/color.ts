const color = {
  accent: '#F87171',
  textBlack: '#000000',
  textLight: '#6D7D8B',
  textDark: '#374151',
  textWhite: '#FFFFFF',
  textBlue: '#27AE60',

  borderColor: '#C3C3C3',

  textLightOp: (color) => (color ? '#6D7D8B' + color.toString() : '#6D7D8B'),
  textBlueOp: (color) => (color ? '#27AE60' + color.toString() : '#27AE60'),
  textDarkOp: (color) => (color ? '#374151' + color.toString() : '#374151'),
  textBlackOp: (color) => (color ? '#27AE60' + color.toString() : '#27AE60'),
  textWhiteOp: (color) => (color ? '#FFFFFF' + color.toString() : '#FFFFFF'),

  borderColorOp: (color) => (color ? '#C3C3C3' + color.toString() : '#C3C3C3'),

  greyLight: '#c0c0c0',
  greyDark: '#ADA9B3',
  appLight: '#3997E6',

  // background: #27AE60;

  greyLightOp: (color) => (color ? '#c0c0c0' + color.toString() : '#c0c0c0'),
  greyDarkOP: (color) => (color ? '#ADA9B3' + color.toString() : '#ADA9B3'),
  appLightOp: (color) => (color ? '#3997E6' + color.toString() : '#3997E6'),

  primary: '#27AE60',
  appColor: '#27AE60',
  app: '#27AE60',
  dark: '#131313',
  appDark: '#703FAE',
  light: '#6D7D8B',

  bg: '#FFFFFF',
  bgDark: '#F9FAFB',

  bgLight: '#f0f0f0',

  buttonColor: '#60D18D',
  buttonBg: '#60D18D',
  black: '#000000',
  white: '#FFFFFF',

  primaryOp: (color) => (color ? '#27AE60' + color.toString() : '#27AE60'),
  appColorOp: (color) => (color ? '#27AE60' + color.toString() : '#27AE60'),
  appOp: (color) => (color ? '#CEDEEE' + color.toString() : '#CEDEEE'),
  darkOp: (color) => (color ? '#131313' + color.toString() : '#131313'),
  appDarkOp: (color) => (color ? '#27AE60' + color.toString() : '#27AE60'),
  lightOp: (color) => (color ? '#6D7D8B' + color.toString() : '#6D7D8B'),
  bgOp: (color) => (color ? '#FFFFFF' + color.toString() : '#FFFFFF'),
  bgDarkOp: (color) => (color ? '#F9FAFB' + color.toString() : '#F9FAFB'),
  bgLightOp: (color) => (color ? '#f0f0f0' + color.toString() : '#f0f0f0'),

  buttonColorOp: (color) => (color ? '#60D18D' + color.toString() : '#60D18D'),
  buttonBgOP: (color) => (color ? '#60D18D' + color.toString() : '#60D18D'),
  blackOp: (color) => (color ? '#000000' + color.toString() : '#000000'),
  whiteOp: (color) => (color ? '#FFFFFF' + color.toString() : '#FFFFFF'),
};

const colorDark = {
  accent: '#F87171',
  textBlack: '#F9FAFB',
  textLight: '#f0f0f0',
  textDark: '#F9FAFB',
  textWhite: '#111827',
  textBlue: '#27AE60',
  borderColor: '#BBC0C5',

  // 27AE60

  textLightOp: (color) => (color ? '#f0f0f0' + color.toString() : '#f0f0f0'),
  textBlueOp: (color) => (color ? '#27AE60' + color.toString() : '#27AE60'),
  textDarkOp: (color) => (color ? '#F9FAFB' + color.toString() : '#F9FAFB'),
  textBlackOp: (color) => (color ? '#F9FAFB' + color.toString() : '#F9FAFB'),
  textWhiteOp: (color) => (color ? '#111827' + color.toString() : '#111827'),
  borderColorOp: (color) => (color ? '#BBC0C5' + color.toString() : '#BBC0C5'),

  greyLight: '#c0c0c0',
  greyDark: '#ADA9B3',
  appLight: '#3997E6',

  // background: #27AE60;

  greyLightOp: (color) => (color ? '#c0c0c0' + color.toString() : '#c0c0c0'),
  greyDarkOP: (color) => (color ? '#ADA9B3' + color.toString() : '#ADA9B3'),
  appLightOp: (color) => (color ? '#3997E6' + color.toString() : '#3997E6'),

  primary: '#27AE60',
  appColor: '#27AE60',
  app: '#27AE60',
  dark: '#131313',
  appDark: '#703FAE',
  light: '#6D7D8B',
  bg: '#1F2937',
  bgDark: '#111827',
  bgLight: '#4B5563',
  buttonColor: '#60D18D',
  buttonBg: '#60D18D',
  white: '#111827',
  black: '#F9FAFB',

  primaryOp: (color) => (color ? '#27AE60' + color.toString() : '#27AE60'),
  appColorOp: (color) => (color ? '#27AE60' + color.toString() : '#27AE60'),
  appOp: (color) => (color ? '#CEDEEE' + color.toString() : '#CEDEEE'),
  darkOp: (color) => (color ? '#131313' + color.toString() : '#131313'),
  appDarkOp: (color) => (color ? '#27AE60' + color.toString() : '#27AE60'),
  lightOp: (color) => (color ? '#4B5563' + color.toString() : '#4B5563'),
  bgOp: (color) => (color ? '#1F2937' + color.toString() : '#1F2937'),
  bgDarkOp: (color) => (color ? '#111827' + color.toString() : '#111827'),
  bgLightOp: (color) => (color ? '#1F2937' + color.toString() : '#1F2937'),
  buttonColorOp: (color) => (color ? '#60D18D' + color.toString() : '#60D18D'),
  buttonBgOP: (color) => (color ? '#60D18D' + color.toString() : '#60D18D'),

  blackOp: (color) => (color ? '#F9FAFB' + color.toString() : '#F9FAFB'),
  whiteOp: (color) => (color ? '#111827' + color.toString() : '#111827'),

  // background: #27AE60;
};
export {color, colorDark};

// function(){

// }
