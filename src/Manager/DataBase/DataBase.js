import firebase from 'firebase';
const firebaseConfig = {
  apiKey: 'AIzaSyD_DwbrN5nNWUZL24DEMy1tz86sBb3ucRs',
  authDomain: 'todo-5e045.firebaseapp.com',
  databaseURL: 'https://todo-5e045-default-rtdb.firebaseio.com',
  projectId: 'todo-5e045',
  storageBucket: 'todo-5e045.appspot.com',
  messagingSenderId: '399812150524',
  appId: '1:399812150524:web:e9152a03099cc4fb8e0379',
  measurementId: 'G-J5TX0M7N3T',
};

export default class DataBase {
  constructor() {
    this.db = null;
    this.auth = null;
    this.connection = null;
    this.openFireBaseConnection();
  }
  async openFireBaseConnection() {
    try {
      await this.setFirebaseConnection();
      await this.setAuthConnection();
      await this.setDbConnection();
    } catch (e) {
      console.log(e);
    }
  }
  async setFirebaseConnection() {
    try {
      if (!firebase.apps.length) {
        this.connection = firebase.initializeApp(firebaseConfig);
        if (this.connection) {
          return this.connection;
        }
      }
      if (this.connection) {
        return this.connection;
      }
      this.connection = firebase;
      if (this.connection) {
        return this.connection;
      }
      throw new Error('setFirebaseConnection');
    } catch (e) {
      throw e;
    }
  }
  async getFirebaseConnection() {
    try {
      if (this.connection) {
        return this.connection;
      }
      this.connection = this.setFirebaseConnection();
      if (this.connection) {
        return this.connection;
      }
      throw new Error('getFirebaseConnection');
    } catch (e) {
      throw e;
    }
  }

  async setAuthConnection() {
    try {
      if (this.connection) {
        this.auth = this.connection.auth();
        if (this.auth) {
          return this.auth;
        }
      }
      let connection = await this.getFirebaseConnection();
      if (connection) {
        this.auth = connection.auth();
        if (this.auth) {
          return this.auth;
        }
      }
      throw new Error('setAuthConnection');
    } catch (e) {
      throw e;
    }
  }

  async getAuthConnection() {
    try {
      if (this.auth) {
        return this.auth;
      }
      this.auth = await this.setAuthConnection();
      if (this.auth) {
        return this.auth;
      }
      throw new Error('getAuthConnection');
    } catch (e) {
      throw e;
    }
  }

  async setDbConnection() {
    try {
      if (this.connection) {
        this.db = this.connection.database();
        if (this.db) {
          return this.db;
        }
      }
      let connection = await this.getFirebaseConnection();
      if (connection) {
        this.db = connection.database();
        if (this.db) {
          return this.db;
        }
      }
      throw new Error('setDbConnection');
    } catch (e) {
      throw e;
    }
  }
  async getDbConnection() {
    try {
      if (this.db) {
        return this.db;
      }
      this.db = await this.setDbConnection();
      if (this.db) {
        return this.db;
      }
      throw new Error('getDbConnection');
    } catch (e) {
      console.log(e);
    }
  }

  async add(path, data) {
    try {
      let db = await this.getDbConnection();
      if (db) {
        let response = await db.ref(path).set(data);
        return true;
      }
      throw new Error();
    } catch (e) {
      console.log(e);
    }
  }

  async update(path, data) {
    try {
      let db = await this.getDbConnection();
      if (db) {
        // db.ref('path').update(data)
        let response = await db.ref(path).update(data);
        return response;
      }
    } catch (e) {
      console.log(e);
    }
  }
  // Attach an asynchronous callback to read the data at our posts reference
  async get(path) {
    try {
      let db = await this.getDbConnection();
      if (db) {
        let leadsRef = await db.ref(path).once('value');
        let response = await leadsRef.val();
        return response;
      }
    } catch (e) {
      console.log(e);
    }
  }
  // Attach an asynchronous callback to read the data at our posts reference
  async _get(path) {
    try {
      let db = await this.getDbConnection();
      if (db) {
        let leadsRef = await db.ref(path).on('value');
        let response = await leadsRef.val();
        return response;
      }
    } catch (e) {
      console.log(e);
    }
  }
  async delete(path) {
    try {
      let db = await this.getDbConnection();
      if (db) {
        let response = await db.ref(path).remove();
        return response;
      }
      throw new Error();
    } catch (e) {
      console.log(e);
    }
  }

  async signin(email, password) {
    try {
      let auth = await this.getAuthConnection();
      if (auth) {
        let response = await auth.signInWithEmailAndPassword(email, password);
        return response.user;
      }
      throw new Error('FireBase Error');
    } catch (e) {
      throw e;
    }
  }
  async signup(email, password) {
    try {
      let auth = await this.getAuthConnection();
      if (auth) {
        let response = await auth.createUserWithEmailAndPassword(
          email,
          password,
        );
        return response;
      }
      throw new Error('FireBase Error');
    } catch (e) {
      console.log(e);
    }
  }
}
module.exports = {DataBase};
