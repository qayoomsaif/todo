import { DataBase } from '../Factory'
export const GET_TO_DO_LIST_SUCCESS = "GET_TO_DO_LIST_SUCCESS";
export const GET_USER_INFO_SUCCESS = "GET_USER_INFO_SUCCESS";
export function getTodoList(path) {
  return async dispatch => {
    try {
      let data = await DataBase.get(path)
      let object = await Object.values(data)
      dispatch(changeToDoList(object));
      return data
    } catch (e) {
      // dispatch(fetchProductsFailure(e))
    }
  };
}
export function getUserInfo(path) {
  return async dispatch => {
    try {
      let data = await DataBase.get(path)
      dispatch(changeUserInfo(data));
      return data
    } catch (e) {
      // dispatch(fetchProductsFailure(e))
    }
  };
}
export const changeToDoList = data => {
  return {
    type: GET_TO_DO_LIST_SUCCESS,
    payload: data
  };
};


export const changeUserInfo = data => ({
  type: GET_USER_INFO_SUCCESS,
  payload: data
});
