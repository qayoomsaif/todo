import {
  Router,
  Helper,
  UserInfoStorage,
  SystemStorage,
  DataBase,
} from 'Factory';
import {
  AccessToken,
  GraphRequest,
  GraphRequestManager,
  LoginManager,
} from 'react-native-fbsdk';

export default class LoginController {
  constructor(object) {
    this.object = object;
  }
  // Stores credentials in local storage
  async setUserInfo(email, name, id, type, isLogin) {
    let response = await UserInfoStorage.setUserInfo(
      email,
      name,
      id,
      type,
      isLogin,
    );
    if (response) {
      return true;
    }
    throw {message: 'Something went wrong. Please try again.'};
  }
  async storOnFireBase(email, name, id, type) {
    await DataBase.add('users/' + id, {
      name: name,
      email: email,
      id: id,
      type: type,
    });
  }
  async validation(email, password) {
    if (!email) {
      throw {message: 'Enter email address.'};
    }
    if (!password) {
      throw {message: 'Enter password.'};
    }
    let validateEmail = await Helper.validateEmail(email);
    if (!validateEmail) {
      throw {message: 'Enter a valid email e.g abc@xyz.com'};
    }
    if (password.length < 5) {
      throw {message: 'Enter password.'};
    }
  }
  async signInWithEmail(email, password) {
    try {
      this.object.onLoading(true);
      await this.validation(email, password);
      let response = await DataBase.signin(email, password);
      if (response.uid) {
        let data = await DataBase.get('users/' + 'E' + response.uid);
        await this.setUserInfo(data.email, data.name, data.id, 'email', true);
        let usetInfo = await UserInfoStorage.getUserInfo();
        await this.object.setUserInfoInRedux(usetInfo);
        this.object.onLoading(false);
        this.goToHomeScreen();
        return;
      }
      throw new Error();
    } catch (e) {
      console.log(e);
      this.object.onLoading(false);
      if (e.message) {
        Helper.alert(e.message);
        return;
      }
      Helper.alert('Something went wrong. Please try again.');
      return;
    }
  }

  loginWithFacebook = async () => {
    // Attempt a login using the Facebook login dialog asking for default permissions.
    try {
      this.object.onLoading(true);
      let login = await LoginManager.logInWithPermissions(['public_profile']);
      if (login.isCancelled) {
        this.object.onLoading(false);
        return;
      } else {
        let data = await AccessToken.getCurrentAccessToken();
        let accessToken = await data.accessToken.toString();
        let user = await this.getInfoFromToken(accessToken);
        await this.storOnFireBase(
          'F' + user.id,
          user.name,
          'F' + user.id,
          'facebook',
        );
        await this.setUserInfo(
          'F' + user.id,
          user.name,
          'F' + user.id,
          'facebook',
        );
        let usetInfo = await UserInfoStorage.getUserInfo();
        await this.object.setUserInfoInRedux(usetInfo);
        this.goToHomeScreen();
      }
    } catch (e) {
      console.log(e);

      await UserInfoStorage.userLogout();
      this.object.onLoading(false);
      if (e.message) {
        Helper.alert(e.message);
        return;
      }
      Helper.alert('Something went wrong. Please try again.');
      return;
    }
  };
  getInfoFromToken = async (token) => {
    let promise = new Promise(async (resolve, reject) => {
      const PROFILE_REQUEST_PARAMS = {
        fields: {string: 'id,name,first_name,last_name'},
      };
      const profileRequest = new GraphRequest(
        '/me',
        {token, parameters: PROFILE_REQUEST_PARAMS},
        (error, user) => {
          if (error) {
            return reject(error);
          } else {
            return resolve(user);
          }
        },
      );
      new GraphRequestManager().addRequest(profileRequest).start();
    });
    return promise;
  };

  goToHomeScreen() {
    Router.homeComponent();
  }
  goToSignupComponent() {
    Router.signupComponent();
  }
}
module.exports = {LoginController};
