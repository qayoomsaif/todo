import React, {Component} from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  ActivityIndicator,
  Keyboard,
  TouchableWithoutFeedback,
  Image,
  BackHandler,
  KeyboardAvoidingView,
  TextInput,
} from 'react-native';
import styles from './AddTask.style';
import AddTaskController from './AddTask.controller';
import {Header, LoadingBar, Input} from 'components';
import {connect} from 'react-redux';

import {changeUserInfo, changeToDoList} from 'redux-store/actions';

class AddTaskComponent extends Component {
  controller = new AddTaskController(this);
  state = {
    title: this.props.title,
    des: this.props.des,
    taskId: this.props.taskId,
    prupos: this.props.prupos,
    date: this.props.date,
  };

  componentDidMount() {
    this.controller.getlist();
  }
  setLoading(value) {
    this.setState({loading: value ? value : false});
  }

  upDateToDoList(updateList) {
    this.props.dispatch(changeToDoList(updateList));
  }

  setError(value) {
    this.setState({errType: value ? value : 0});
  }
  render() {
    const {userInfo, toDoList} = this.props;

    const {title, des, prupos, date, taskId, errType, loading} = this.state;
    return (
      <View style={styles.mainContainer}>
        <Header
          title={this.props.screenTitle ? this.props.screenTitle : 'Add Task'}
          onPressBack={() => this.controller.goToBack()}
          backImage={require('src/assets/icon/back.png')}
        />
        <View style={styles.contentContainer}>
          <TextInput
            // stylemain={styles.titleInputMain}
            style={styles.titleInputText}
            value={title}
            onChangeText={(text) => this.setState({title: text})}
            placeholder={'Title*'}
            maxLength={100}
            ref={(ref) => {
              this.title = ref;
            }}
            onSubmitEditing={() => (this.des ? this.des.focus() : null)}
          />
          <TextInput
            // stylemain={styles.DesInputMain}
            style={styles.desInputText}
            value={des}
            multiline={true}
            onChangeText={(text) => this.setState({des: text})}
            placeholder={'Description*'}
            maxLength={500}
            ref={(ref) => {
              this.des = ref;
            }}
            onSubmitEditing={() => console.log(name)}
          />
          <TouchableOpacity
            onPress={() =>
              prupos == 'edit'
                ? this.controller.update(
                    title,
                    des,
                    date,
                    taskId,
                    userInfo,
                    toDoList,
                  )
                : this.controller.add(title, des, userInfo, toDoList)
            }
            style={styles.submitbuttoncontainer}>
            <Text style={styles.submitbuttontext}>Add Task</Text>
          </TouchableOpacity>
        </View>
        {loading ? <LoadingBar style={styles.loading} /> : null}
      </View>
    );
  }
}
const mapStateToProps = ({account}) => {
  const {userInfo, toDoList} = account;
  return {userInfo, toDoList};
};

export default connect(mapStateToProps)(AddTaskComponent);
