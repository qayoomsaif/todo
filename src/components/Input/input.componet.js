import React, {Component} from 'react';
import {View, TextInput} from 'react-native';
import Keybords from './keybord';
export default class Input extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View
        style={
          this.props.stylemain || {
            height: '100%',
            width: '100%',
            justifyContent: 'center',
          }
        }>
        <Keybords
          onToggle={(status, height) => {
            this.props.info ? this.props.info(status, height) : null;
          }}
        />
        <TextInput
          placeholderTextColor={this.props.placeholderTextColor || null}
          style={this.props.style || null}
          placeholder={this.props.placeholder}
          onChangeText={(text) =>
            this.props.onChangeText ? this.props.onChangeText(text) : null
          }
          value={this.props.value}
          selectionColor={this.props.selectionColor || null}
          placeholderTextColor={this.props.placeholderTextColor || '#00000050'}
          keyboardType={this.props.keyboardType || 'default'}
          underlineColorAndroid={'transparent'}
          returnKeyType={this.props.returnKeyType || 'default'}
          multiline={this.props.multiline || false}
          autoCapitalize={this.props.autoCapitalize || 'none'}
          autoCorrect={this.props.autoCorrect || false}
          secureTextEntry={this.props.secureTextEntry || false}
          onFocus={this.props.onFocus}
          autoFocus={this.props.autoFocus || false}
          spellCheck={this.props.spellCheck || false}
          maxLength={this.props.maxLength || '10000'}
          defaultValue={this.props.defaultValue || false}
          editable={this.props.editable}
          onSubmitEditing={this.props.onSubmitEditing || null}
          ref={(ref) => (this.props.refs ? this.props.refs(ref) : null)}
        />
      </View>
    );
  }
}
module.exports = {Input};
