import React, {Component} from 'react';
import {
  Text,
  View,
  KeyboardAvoidingView,
  TouchableOpacity,
  Image,
  Keyboard,
  TextInput,
} from 'react-native';
import SignupController from './Signup.controller';
import styles from './Signup.style';
import {LoadingBar, Input} from 'src/components/';
import {connect} from 'react-redux';
import {changeUserInfo} from 'redux-store/actions';
class SignupComponent extends Component {
  controller = new SignupController(this);
  state = {
    email: null,
    name: null,
    password: null,
    loading: false,
  };

  onLoading(value) {
    this.setState({loading: value || false});
  }
  setUserInfoInRedux(respose) {
    this.props.dispatch(changeUserInfo(respose));
  }

  render() {
    const {email, name, loading, password} = this.state;
    return (
      <View style={styles.maincontainer}>
        <View style={styles.header}>
          <TouchableOpacity
            style={styles.crossbutton}
            onPress={() => this.controller.goToBack()}>
            <Image
              style={styles.crossbuttonimage}
              source={require('src/assets/icon/cross.png')}
            />
          </TouchableOpacity>
        </View>

        <View style={styles.logoimagecontainer}>
          <Image
            style={styles.logoimage}
            source={require('src/assets/icon/logo.png')}
          />
        </View>
        <Text style={styles.headingtext}> Sign Up</Text>
        <View style={styles.inputemailcontainer}>
          <TextInput
            style={styles.inputemailtext}
            placeholder={'Enter your name'}
            onChangeText={(text) => this.setState({name: text})}
            value={name}
            maxLength={100}
            returnKeyType={'next'}
            onSubmitEditing={() =>
              this.emailInput ? this.emailInput.focus() : null
            }
            ref={(ref) => {
              this.nameInput = ref;
            }}
          />
        </View>

        <TextInput
          style={styles.inputemailtext}
          placeholder={'Enter your email'}
          onChangeText={(text) => this.setState({email: text.toLowerCase()})}
          value={email}
          maxLength={100}
          onSubmitEditing={() =>
            this.passwordInput ? this.passwordInput.focus() : null
          }
          ref={(ref) => {
            this.emailInput = ref;
          }}
        />

        <TextInput
          style={styles.inputemailtext}
          placeholder={'Enter your Password'}
          onChangeText={(text) => this.setState({password: text})}
          value={password}
          secureTextEntry={true}
          maxLength={100}
          onSubmitEditing={() =>
            this.controller.handleSignUp(name, email, password)
          }
          ref={(ref) => {
            this.passwordInput = ref;
          }}
        />

        <TouchableOpacity
          style={styles.continuebuttoncontainer}
          onPress={() => {
            Keyboard.dismiss();
            this.controller.handleSignUp(name, email, password);
          }}>
          <Text style={styles.continuebuttontext}>Signup</Text>
        </TouchableOpacity>

        {loading ? (
          <View style={styles.loadingcontainer}>
            <LoadingBar />
          </View>
        ) : null}
      </View>
    );
  }
}
const mapStateToProps = ({account}) => {
  const {userInfo, toDoList} = account;
  return {userInfo, toDoList};
};
export default connect(mapStateToProps)(SignupComponent);
