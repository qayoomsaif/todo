import { GET_TO_DO_LIST_SUCCESS, GET_USER_INFO_SUCCESS } from './actions';
const INITIAL_STATE = { toDoList: [], userInfo: null, error: null };
export default (state = INITIAL_STATE, action, ) => {
  switch (action.type) {
    case GET_TO_DO_LIST_SUCCESS:
      return { ...state, toDoList: action.payload }
    case GET_USER_INFO_SUCCESS:
      return { ...state, userInfo: action.payload }
    default:
      return state;
  }
}
