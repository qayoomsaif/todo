import {
  Helper, Router, DataBase,
} from "src/Factory";
var uuid = require('react-native-uuid');
var moment = require('moment');

export default class AddTaskController {
  constructor(object) {
    this.object = object;

  }
  async validation(title, des, ) {
    if (!title) {
      Helper.alert('Please enter Title'); return false
    }
    if (!des) {
      Helper.alert('Please enter Des')
      return false
    }
    return true
  }
  async getlist() {
    try {
      let response = await DataBase.get()
    } catch (e) { }
  }
  async add(title, des, userInfo, toDoList) {
    try {
      this.object.setLoading(true)
      let vali = await this.validation(title, des)
      if (vali) {
        let id = await uuid.default.v4();
        let para = { title: title, des: des, date: moment().format("YYYY-MM-DD HH:mm:ss"), id: id }
        await DataBase.add('to_do_list' + '/' + userInfo.id + '/' + id, para)
        let list = [...toDoList, para]
        await this.object.upDateToDoList(list)
        this.goToBack()
      }
      this.object.setLoading(false)
    }
    catch (e) {
      console.log(e)
    }

  }
  async update(title, des, date, taskId, userInfo, toDoList) {
    try {
      this.object.setLoading(true)
      let vali = await this.validation(title, des)
      if (vali) {
        await DataBase.update('to_do_list' + '/' + userInfo.id + '/' + taskId, { title: title, des: des, date: date, id: taskId })
        let list = await toDoList.map(res => {
          if (res.id == taskId) { return { title: title, des: des, date: date, id: taskId } }
          else { return res }
        })
        await this.object.upDateToDoList(list)
        this.goToBack()
      }
      this.object.setLoading(false)
    }
    catch (e) {
      console.log(e)
    }
  }
  goToBack() { Router.back() }
}
