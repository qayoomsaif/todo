import React from 'react';
import { Scene, Router } from 'react-native-router-flux';
import SplashComponent from 'src/Screen/Splash/splash.component';
import HomeComponent from 'src/Screen/Home/home.component';
import AddTaskComponent from 'src/Screen/AddTask/AddTask.component';
import ViewTaskComponent from 'src/Screen/ViewTask/ViewTask.component';
import LoginComponent  from 'src/Screen/User/Login/login.component'
import SignupComponent from 'src/Screen/User/Signup/Signup.component'
import UserProfileComponent from 'src/Screen/User/UserProfile/UserProfile.component'
const Routes = () => {
    return (
        <Router>
            <Scene key="root">
                <Scene key="splashComponent" component={SplashComponent} hideNavBar={true} panHandlers={null} initial />
                <Scene key="homeComponent" component={HomeComponent} hideNavBar={true} panHandlers={null}  />
                <Scene key="loginComponent" component={LoginComponent} hideNavBar={true} panHandlers={null}  />
                <Scene key="signupComponent" component={SignupComponent} hideNavBar={true} panHandlers={null}  />
                <Scene key="addTaskComponent" component={AddTaskComponent} hideNavBar={true} panHandlers={null}  />
                <Scene key="viewTaskComponent" component={ViewTaskComponent} hideNavBar={true} panHandlers={null}  />
                <Scene key="userProfileComponent" component={UserProfileComponent} hideNavBar={true} panHandlers={null}  />
            </Scene>
        </Router>
    );
}

module.exports = { Routes };


{/* <Filter
                    date={this.state.date}
                    interest={this.props.advanceFilterData}
                    city={this.state.city}
                /> */}