import {Platform} from 'react-native';
import {StyleSheet} from 'react-native';

import {
  font,
  resize,
  color,
  heightScale,
  widthScaleSub,
  widthScale,
  width,
  height,
  square,
} from 'globalStyle';

export default styles = StyleSheet.create({
  maincontainer: {
    flex: 1,
    backgroundColor: color.bgLight,
  },
  logoDiscContainer: {
    alignItems: 'center',
    marginTop: heightScale(45),
  },
  logoicon: {
    marginTop: heightScale(200),
    width: square(100),
    height: square(100),
  },
  subtext: {
    marginTop: heightScale(45),
    fontFamily: font.two.bold,
    fontSize: widthScale(20),
    textAlignVertical: 'center',
    textAlign: 'center',
    color: color.app,
  },
  imagecontainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    height: heightScale(160),
  },
  image: {
    width: width,
    height: heightScale(160),
  },
});
