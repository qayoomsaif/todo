import React, { Component } from 'react';
import { View, Text, BackHandler } from 'react-native';
import styles from "./UserProfile.style";
import UserProfileController from "./UserProfile.controller";
import { Header, LoadingBar, } from 'components'
import { connect } from 'react-redux';
import { changeUserInfo, changeToDoList } from 'redux-store/actions';
class UserProfileComponent extends Component {
  controller = new UserProfileController(this)
  state = {
  }
  componentDidMount() { BackHandler.addEventListener('UserProfileComponent', this.handleBackButton.bind(this)) }
  componentWillUnmount() { BackHandler.removeEventListener('UserProfileComponent', this.handleBackButton.bind(this)) }
  handleBackButton() { this.controller.goToBack(); return true }
  render() {
    const { userInfo } = this.props
    return (
      <View style={styles.mainContainer}>
        <Header
          title={'Profile'}
          onPressBack={() => this.handleBackButton()}
          backImage={require('src/assets/icon/back.png')}
        />
        <View style={styles.contentContainer}>
          <Text style={styles.titleText}>
            {"Name :   "}<Text style={styles.text}>
              {userInfo ? userInfo.name : null}
            </Text>
          </Text>
          <Text style={styles.titleText}>
            {"Email :   "}<Text style={styles.text}>
            {userInfo ? userInfo.email : null}
          </Text>
          </Text>
          
        </View>
      </View>
    );
  }
}
const mapStateToProps = ({ account }) => {
  const { userInfo, toDoList } = account;
  return { userInfo, toDoList };
};
export default connect(mapStateToProps)(UserProfileComponent);