import {StyleSheet} from 'react-native';
import {Platform} from 'react-native';

import {
  font,
  resize,
  color,
  heightScale,
  widthScaleSub,
  widthScale,
  width,
  height,
  square,
} from 'globalStyle';

export default styles = StyleSheet.create({
  topcontainer: {
    flex: 1,
  },
  topcontainerimage: {
    position: 'absolute',
    bottom: 0,
    top: 0,
    backgroundColor: color.bgDark,
    width: width,
    alignContent: 'center',
  },
  topTitleBlock: {
    marginTop: heightScale(30),
    width: widthScaleSub(40),
    alignItems: 'center',
    paddingVertical: heightScale(10),
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'space-around',
    borderColor: color.white,
    borderWidth: 2,
    borderRadius: square(6),
  },
  topTitle: {
    borderColor: '#FFF',
    paddingHorizontal: widthScale(10),
  },
  topTitleText1: {
    fontWeight: 'bold',
    fontSize: widthScale(14),
    color: '#ffffff',
  },
  BottomBlock: {
    position: 'absolute',
    bottom: Platform.OS == 'android' ? 0 : 20,
    left: 0,
    right: 0,
  },
  topcontainerfooter: {
    width: widthScaleSub(40),
    marginBottom: heightScale(50),
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: square(5),
    height: heightScale(60),
    backgroundColor: color.appColor,
  },
  topcontainerfooterbottontext: {
    fontSize: widthScale(18),
    color: color.white,
    textAlign: 'center',
  },
});
