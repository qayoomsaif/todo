import {
  Helper, Router, DataBase
} from "src/Factory";
import { Services, error } from 'src/Services'
export default class ViewTaskController {
  constructor(object) {
    this.object = object;
  }

  goToBack() { Router.back() }
  goToEdit(item) { Router.addTaskComponent({ title: item.title, des: item.des, taskId: item.id, date: item.date, prupos: 'edit' }) }
  goToAdd() { Router.addTaskComponent() }
  async delete(item, user, list) {
    try {
      await DataBase.delete('to_do_list' + '/' + user.id + '/' + item.id)
      var updateList = list.filter(e => e.id != item.id)
      this.object.upDateToDoList(updateList)
    } catch (e) {
      console.log(e)
    }
  }



}
