import {StyleSheet} from 'react-native';

import {
  font,
  resize,
  color,
  heightScale,
  widthScaleSub,
  widthScale,
  width,
  height,
  square,
} from 'globalStyle';

export default styles = StyleSheet.create({
  maincontainer1: {
    flex: 1,
    alignItems: 'flex-start',
  },
  maincontainer: {
    flex: 1,
    width: width - width / 10,
    backgroundColor: color.bgDark,
    alignItems: 'center',
  },
  scrollview: {
    width: '100%',
  },
  menucontainer: {
    marginTop: heightScale(40),
    width: '100%',
    flex: 1,
  },
  menulistitem: {
    marginRight: widthScale(20),
    marginLeft: widthScale(20),

    alignItems: 'center',
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: '#70707080',
    paddingVertical: heightScale(15),
  },
  menulistitem2: {
    marginRight: widthScale(20),
    marginLeft: widthScale(20),
    marginTop: heightScale(10),

    marginBottom: heightScale(100),

    alignItems: 'center',
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: '#70707080',
    paddingVertical: heightScale(15),
  },
  menulistitem1: {
    marginHorizontal: widthScale(0),
    paddingHorizontal: widthScale(20),
    backgroundColor: '#006AFF',

    flexDirection: 'row',
    alignItems: 'center',

    borderBottomWidth: 0.5,
    borderBottomColor: '#70707080',
    paddingVertical: heightScale(15),
  },

  listmenuiconimage: {
    width: square(23),
    height: square(23),
  },
});
