import {Service} from 'Services/MainService/MainService';
import CustomError from 'src/CustomError';

import UserServices from './UserServices';

var Services = {
  userServices: new UserServices(Service),
};

module.exports = {
  services: Services,
  Services: Services,
  error: CustomError,
};
