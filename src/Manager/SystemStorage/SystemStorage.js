class SystemStorage {
    constructor(storageManager, defaultData) {
        this.storageManager = storageManager;
        this.defaultData = defaultData
    }

    isEmpty(object) {
        var isEmpty = true;
        for (keys in object) {
            isEmpty = false;
            break; // exiting since we found that the object is not empty
        }
        return isEmpty;
    }
}
module.exports = { SystemStorage }