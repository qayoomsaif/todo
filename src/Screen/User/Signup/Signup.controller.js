import {Router, Helper, UserInfoStorage, DataBase} from 'Factory';
export default class SignupController {
  constructor(object) {
    this.object = object;
  }

  setStatus(status) {
    // console.log(status);
  }
  async setUserInfo(email, name, id, type, isLogin) {
    let response = await UserInfoStorage.setUserInfo(
      email,
      name,
      id,
      type,
      isLogin,
    );
    if (response) {
      return true;
    }
    throw {message: 'Something went wrong. Please try again.'};
  }
  async validation(name, email, password) {
    if (!name) {
      await Helper.alert('Enter name.');
      return false;
    }
    if (!email) {
      await Helper.alert('Enter email address.');
      return false;
    }
    if (!password) {
      await Helper.alert('Enter password.');
      return false;
    }
    let validateEmail = await Helper.validateEmail(email);
    if (!validateEmail) {
      await Helper.alert('Enter a valid email e.g abc@xyz.com');
      return false;
    }
    if (password.length < 5) {
      await Helper.alert('Password should be at least 6 characters');
      return false;
    }
    return true;
  }
  async handleSignUp(name, email, password) {
    try {
      this.object.onLoading(true);
      let validation = await this.validation(name, email, password);
      if (validation) {
        let response = await DataBase.signup(email, password);
        if (response) {
          let responseLogin = await DataBase.signin(email, password);
          await this.storOnFireBase(
            email,
            name,
            'E' + responseLogin.uid,
            'email',
          );
          await this.setUserInfo(
            email,
            name,
            'E' + responseLogin.uid,
            'email',
            true,
          );
          let usetInfo = await UserInfoStorage.getUserInfo();
          await this.object.setUserInfoInRedux(usetInfo);
          this.object.onLoading(false);
          this.goToHomeScreen();
        }
        this.object.onLoading(false);
      }
      this.object.onLoading(false);
    } catch (e) {
      console.log(e)
      await UserInfoStorage.userLogout();
      this.object.onLoading(false);
      if (e.message) {
        Helper.alert(e.message);
        return;
      }
      Helper.alert('Something went wrong. Please try again.');
      return;
    }
  }
  async storOnFireBase(email, name, id, type) {
    await DataBase.add('users/' + id, {
      name: name,
      email: email,
      id: id,
      type: type,
    });
  }

  goToBack() {
    Router.back();
  }
  goToHomeScreen() {
    Router.homeComponent();
  }
}
