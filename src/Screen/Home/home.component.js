import React, {Component} from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  BackHandler,
  ImageBackground,
} from 'react-native';
import styles from './home.style';
import {Header, Drawer} from 'components';
import MenuDrawer from 'react-native-side-drawer';
import HomeController from './home.controller';
import {connect} from 'react-redux';
import {changeUserInfo, changeToDoList} from 'redux-store/actions';
class HomeComponent extends Component {
  controller = new HomeController(this);
  state = {
    open: false,
  };
  componentDidMount() {
    BackHandler.addEventListener('HomeComponent', () => BackHandler.exitApp());
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('HomeComponent');
  }
  handleBackButton() {
    BackHandler.exitApp();
    return true;
  }
  setUserInfoInRedux(respose) {
    this.props.changeUserInfo(respose);
  }
  upDateToDoList(updateList) {
    this.props.changeToDoList(updateList);
    // console.log(this.props.toDoList);
  }
  toggleOpen() {
    if (this.state.open) {
      this.setState({open: false});
      return;
    }
    this.setState({open: true});
  }

  drawerContent(userInfo) {
    return (
      <Drawer
        userInfo={userInfo}
        onPressLogOut={() => this.controller.logOut()}
        onPressBackButton={() => this.setState({open: false})}
      />
    );
  }

  render() {
    const {userInfo} = this.props;
    return (
      <MenuDrawer
        open={this.state.open}
        drawerContent={this.drawerContent(userInfo)}
        drawerPercentage={100}
        animationTime={250}
        overlay={true}
        opacity={1}>
        <Header
          title={'Activity details'}
          onPressBack={() => this.toggleOpen()}
          multipleOption={true}
          backImage={
            this.state.open
              ? require('src/assets/icon/cross.png')
              : require('src/assets/icon/menu.png')
          }
        />
        <View style={styles.topcontainer}>
          <ImageBackground
            source={require('src/assets/icon/image-homepage.jpg')}
            resizeMode={'stretch'}
            style={styles.topcontainerimage}>
            <View style={styles.topTitleBlock}>
              <View style={styles.topTitle}>
                <Text style={styles.topTitleText1}>
                  Best Place Maintain To Do List
                </Text>
              </View>
            </View>
            <View style={styles.BottomBlock}>
              <TouchableOpacity
                style={styles.topcontainerfooter}
                onPress={() => this.toggleOpen()}>
                <Text style={styles.topcontainerfooterbottontext}>Explore</Text>
              </TouchableOpacity>
            </View>
          </ImageBackground>
        </View>
      </MenuDrawer>
    );
  }
}
const mapStateToProps = ({account}) => {
  const {userInfo, toDoList} = account;
  return {userInfo, toDoList};
};
export default connect(mapStateToProps, {changeUserInfo, changeToDoList})(
  HomeComponent,
);
