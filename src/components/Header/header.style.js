import {StyleSheet} from 'react-native';

import {
  font,
  resize,
  color,
  heightScale,
  widthScaleSub,
  widthScale,
  width,
  height,
  square,
} from 'globalStyle';

export default styles = StyleSheet.create({
  maincontiner: {
    flex: 1,
    backgroundColor: color.bgDark,
    width: width,
   
  },

  submaincontiner: {
    flexDirection: 'row',
    width: width,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: color.bgDark,
  },
  backmaincontiner: {
    // height: heightScale(60),
    height: heightScale(60),
  },
  backcontiner: {
    flex: 1,
    marginLeft: widthScale(14),
    justifyContent: 'center',
  },
  backimage: {
    height: heightScale(25),
    width: heightScale(25),
  },
  logoimage: {
    height: heightScale(30),
    width: heightScale(30),
  },
  titlecontiner: {
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingRight: widthScale(15),
  },
  titleText: {
    fontSize: heightScale(16),
    color: color.textDark,
    fontFamily: font.one.semiBold,
  },
});
