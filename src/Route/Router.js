import {Actions} from 'react-native-router-flux';
export default class Router {
  constructor() {
    this.lastScreen = null;
  }
  async stroeScreen(currentScene) {
    this.lastScreen = Actions.currentScene;
    this.currentScene = currentScene;
  }

  async loginComponent() {
    await this.stroeScreen('loginComponent');
    Actions.loginComponent({type: 'reset'});
  }
  async signupComponent(data) {
    await this.stroeScreen('signupComponent');
    Actions.signupComponent(data);
  }
  async homeComponent() {
    await this.stroeScreen('homeComponent');
    Actions.homeComponent({type: 'reset'});
  }
  async addTaskComponent(data) {
    await this.stroeScreen('addTaskComponent');
    Actions.addTaskComponent(data);
  }
  async viewTaskComponent(data) {
    await this.stroeScreen('viewTaskComponent');
    Actions.viewTaskComponent(data);
  }
  async userProfileComponent(data) {
    await this.stroeScreen('userProfileComponent');
    Actions.userProfileComponent(data);
  }
  back(data) {
    if (data) {
      Actions.pop({refresh: {data, refreshs: new Date()}});
      return;
    }
    Actions.pop();
  }
  Popdata(data) {
    if (data) {
      return {refresh: {data}};
    }
    return data;
  }
  backTo(name) {
    Actions.popTo(name);
  }
  push(name, para) {
    Actions.push(name, para);
  }
  refresh(para) {
    Actions.refresh(para);
  }
  prevState() {
    if (Actions.prevState.routes) {
      return Actions.prevState.routes.map(function (i) {
        return i.routeName;
      });
    }
    return false;
  }
  screen() {
    return Actions.currentScene;
  }
  state() {
    if (Actions.state.routes) {
      return Actions.state.routes.map(function (i) {
        return i.routeName;
      });
    }
    return false;
  }
  lastScene() {
    return Actions.prevScene ? Actions.prevScene : false;
  }
}
module.exports = {Router};
