import {
  Helper,
} from "src/Factory";
import { Services, error } from "src/Services";
import { Router } from 'Factory';

export default class DrawerController {
  constructor(object) {
    this.object = object;
  }
  goToNextScreen(name) {
    if (name == 'View Task') {
      Router.viewTaskComponent()
    }
    if (name == 'Add Task') {
      Router.addTaskComponent()
    }
    if (name == 'Home') {
      Router.homeComponent()
    }
    if (name == 'User Profile') {
      Router.userProfileComponent()
    }
  }
}
