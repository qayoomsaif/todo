import {StyleSheet} from 'react-native';

import {
  font,
  resize,
  color,
  heightScale,
  widthScaleSub,
  widthScale,
  width,
  height,
  square,
} from 'globalStyle';

export default styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: color.bgDark,
  },
  contentContainer: {
    marginTop: resize.heightScale(20),
    paddingHorizontal: widthScale(20),
  },

  titleText: {
    fontFamily: font.two.bold,
    fontSize: heightScale(15),
    color: color.textDark,
    marginVertical: resize.heightScale(5),
  },
  text: {
    fontFamily: font.two.semiBold,
    fontSize: heightScale(15),
    color: color.textDark,
  },
});
