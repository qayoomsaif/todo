import {Router, UserInfoStorage} from 'src/Factory';
export default class SplashController {
  constructor(object) {
    this.object = object;
  }
  async loadInformation() {
    let respose = await UserInfoStorage.getUserInfo();
    if (respose) {
      await this.object.setUserInfoInRedux(respose);
      Router.homeComponent();
      return;
    }
    Router.loginComponent();
  }
}
