export default class UserInfoStorage {
  constructor(storageManager) {
    this.storageManager = storageManager;
  }
  async set(object) {
    await this.storageManager._set('userInfo', object ? object : false)
    return object
  }
  async get() {
    let response = await this.storageManager._get('userInfo')
    return response ? response : null
  }

  async setUserInfo(email, name, id, type, isLogin) {
    let object = { email: email, name: name, type: type, id: id, isLogin: isLogin ? isLogin : true, }
    let response2 = await this.set(object)
    return response2;
  }
  async getUserInfo() {
    let response = await this.get()
    return response;
  }
  async getIsLogin() {
    let response = await this.storageManager._get('userInfo')
    if (response) { return response.isLogin }
    return false
  }



  async getUserId() {
    let response = await this.get()
    if (response) { return response.id }
    return false
  }

  async userLogout() {
    let response = await this.storageManager.remove("userInfo");
    return response;
  }
}
module.exports = { UserInfoStorage };
