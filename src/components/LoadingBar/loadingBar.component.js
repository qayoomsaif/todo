import React from 'react';
import {View, ActivityIndicator, Text} from 'react-native';
import styles from './loadingbar.style';
import CardView from 'react-native-cardview';
import {font, resize, color} from 'globalStyle';

const LoadingBar = ({size, color, colorText, text, style}) => {
  return (
    <View style={style || styles.spinnerStyle}>
      <CardView
        cardElevation={2}
        cardMaxElevation={2}
        cornerRadius={styles.backcolor.borderRadius}
        style={styles.backcolor}>
        <ActivityIndicator
          style={styles.stylecomponent}
          color={color || 'grey'}
          size={size || 'large'}
        />
        {text && (
          <Text
            style={{
              color: colorText || '#000',
              fontFamily: font.one.regular,
              textAlign: 'center',
            }}>
            {text}
          </Text>
        )}
      </CardView>
    </View>
  );
};
export {LoadingBar};
