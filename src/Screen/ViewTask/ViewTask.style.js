import {StyleSheet} from 'react-native';

import {
  font,
  resize,
  color,
  heightScale,
  widthScaleSub,
  widthScale,
  width,
  height,
  square,
} from 'globalStyle';

export default styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: color.bgDark,
  },
  addMainContainer: {
    width: widthScaleSub(40),
    borderRadius: square(5),
    marginVertical: heightScale(10),
    alignSelf: 'center',
  },
  addSubContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: heightScale(10),
    backgroundColor: color.bgDark,

  },

  addText: {
    color: color.app,
    fontSize: widthScale(30),
  },

  listItemMainContainer: {
    width: widthScaleSub(40),
    borderRadius: square(10),
    marginVertical: heightScale(10),
    alignSelf: 'center',
    backgroundColor: color.bgDark,
  },
  listItemSubContainer: {
    margin: 0,
    justifyContent: 'center',
    paddingHorizontal: widthScale(20),
    paddingVertical: heightScale(15),
  },

  listItemTitleText: {
    // color: color.app,
    color: color.textDark,

    marginRight: widthScale(50),
    fontFamily: font.one.bold,
    fontSize: heightScale(16),
    marginBottom: heightScale(5),
  },

  listItemDesText: {
    fontFamily: font.one.regular,
    fontSize: heightScale(14),
    color: color.textLight,
  },

  dateText: {
    position: 'absolute',
    right: widthScale(20),
    bottom: heightScale(10),
    fontFamily: font.one.regular,
    fontSize: heightScale(10),
    color: color.textLight,

  },
  listItemButtonMainContainer: {
    position: 'absolute',
    right: square(10),
    top: square(15),
    flexDirection: 'row',
  },

  listItemButtonContainer: {
    alignItems: 'center',
    width: square(20),
    height: square(20),
    marginHorizontal: widthScale(5),
  },

  listItemButtonIcon: {
    width: square(15),
    height: square(15),
  },
});
