import ApplicationFactory from './ApplicationFactory'
import config from "./config"
const factory = ApplicationFactory(config)

// Whenever we need a Manager function, we will import factory and write the name of the desired key. In this way we will be able to call any manager function via factory!
module.exports = {
    Router: factory.getRouter(),
    DataBase: factory.getDataBase(),
    Helper: factory.getHelper(),
    StorageManager: factory.getStorageManager(),
    SystemStorage: factory.getSystemStorage(),
    UserInfoStorage: factory.getUserInfoStorage(),
    GlobalConfig : config.defaultData
    
}

