// // import React, { Component } from 'react';
// // import { Routes } from './index';

// // class App extends Component {
// //     render() {
// //         return (
// //             <Routes />
// //         );
// //     }
// // }

// // export default App;

// import ReactDOM from "react-dom";
// import { createStore, applyMiddleware } from "redux";
// import reduxThunk from "redux-thunk";
// import React, { Component } from 'react';
// import { Routes } from './index';
// import { Provider } from 'react-redux';
// import reducers from 'src/reducers';

// class App extends Component {
//     render() {
//         return (
//             // <View style={{flex: 1}}>

//             <Provider store={createStore(reducers)}>

//                 <Routes />
//             </Provider>
//             //   </View>

//         );
//     }
// }

// export default App;

import React from 'react';
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import { Routes } from './index';
import thunk from "redux-thunk";
import rootReducer from "redux-store/rootReducer";
const store = createStore(rootReducer, applyMiddleware(thunk));
export default class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <Routes />
            </Provider>
        );
    }
}