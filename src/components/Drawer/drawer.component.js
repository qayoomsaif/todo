import React, {Component} from 'react';
import {View, TouchableOpacity, Text, FlatList, ScrollView} from 'react-native';
import styles from './drawer.style';
import DrawerController from './drawer.controller';
import {Router} from 'Factory';
export default class Drawer extends Component {
  constructor(props) {
    super(props);
    this.controller = new DrawerController(this);
    this.state = {
      searchtext: null,
      isLogin: true,
      menuList: [
        // {name: 'Home', image: require('src/assets/icon/home.png')},
        {name: 'Add Task', image: require('src/assets/icon/home.png')},
        {name: 'View Task', image: require('src/assets/icon/home.png')},
        {name: 'User Profile', image: require('src/assets/icon/home.png')},
        {name: 'log out', image: require('src/assets/icon/home.png')},
      ],
    };
  }
  onPressLogOut() {}
  _renderRow(item) {
    return (
      <TouchableOpacity
        style={styles.menulistitem}
        onPress={() => {
          if (item.name == 'log out') {
            this.props.onPressLogOut ? this.props.onPressLogOut() : null;
          } else {
            this.controller.goToNextScreen(item.name);
          }
        }}>
        {/* <Image
          source={item.image}
          resizeMode={'contain'}
          style={styles.listmenuiconimage}
        /> */}
        <Text style={styles.listmenutitletext}>{item.name}</Text>
      </TouchableOpacity>
    );
  }
  render() {
    const {isLogin} = this.state;
    return (
      <TouchableOpacity
        activeOpacity={1.0}
        onPress={() =>
          this.props.onPressBackButton
            ? this.props.onPressBackButton()
            : Router.back()
        }
        style={styles.maincontainer1}>
        <View style={styles.maincontainer}>
          <ScrollView style={styles.scrollview}>
            <View style={styles.menucontainer}>
              <FlatList
                data={this.state.menuList}
                renderItem={(item) => this._renderRow(item.item, item.index)}
                keyExtractor={(item) => item.index}
              />
            </View>
          </ScrollView>
        </View>
      </TouchableOpacity>
    );
  }
}

module.exports = {Drawer};
