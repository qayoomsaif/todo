import {StyleSheet} from 'react-native';

import {
  font,
  resize,
  color,
  heightScale,
  widthScaleSub,
  widthScale,
  width,
  height,
  square,
} from 'globalStyle';

export default styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: color.bgDark,
  },
  contentContainer: {
    marginTop: heightScale(30),
    alignItems: 'center',
    backgroundColor: color.bgDark,
  },

  titleInputText: {
    height: heightScale(50),
    fontFamily: font.one.bold,
    fontSize: heightScale(15),
    color: color.textDark,
    backgroundColor: color.bgLight,
    paddingHorizontal: widthScale(15),
    width: widthScaleSub(40),
    justifyContent: 'center',
    borderRadius: square(4),
  },
  desInputText: {
    fontFamily: font.one.semiBold,
    fontSize: heightScale(15),
    textAlignVertical: 'top',
    marginTop: heightScale(20),
    height: heightScale(200),
    paddingHorizontal: widthScale(15),
    paddingVertical: heightScale(20),
    width: widthScaleSub(40),
    justifyContent: 'center',
    backgroundColor: color.bgLight,
    borderRadius: square(4),
  },
  submitbuttoncontainer: {
    marginTop: heightScale(30),
    height: heightScale(60),
    alignSelf: 'center',
    width: widthScaleSub(40),
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: square(4),
    backgroundColor: color.appColor,
  },
  submitbuttontext: {
    fontSize: heightScale(16),
    fontFamily: font.one.bold,
    color: '#ffffff',
  },
  loading: {
    position: 'absolute',
    bottom: 0,
    top: heightScale(50),
    left: 0,
    right: 0,
    height: height - heightScale(100),
    alignItems: 'center',
    justifyContent: 'center',
  },
});
