export const font = {
  one: {
    regular: 'FiraSans-Regular',
    medium: 'FiraSans-Medium',
    semiBold: 'FiraSans-SemiBold',
    bold: 'FiraSans-SemiBold',
  },
  two: {
    regular: 'SourceSansPro-Regular',
    medium: 'SourceSansPro-Light',
    semiBold: 'SourceSansPro-SemiBold',
    bold: 'SourceSansPro-Bold',
  },
  fontOne: {
    regular: 'FiraSans-Regular',
    medium: 'FiraSans-Medium',
    semiBold: 'FiraSans-SemiBold',
    bold: 'FiraSans-SemiBold',
  },
  fontTwo: {
    regular: 'SourceSansPro-Regular',
    medium: 'SourceSansPro-Light',
    semiBold: 'SourceSansPro-SemiBold',
    bold: 'SourceSansPro-Bold',
  },
};
