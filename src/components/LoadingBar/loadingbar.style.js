import {StyleSheet} from 'react-native';

import {
  font,
  resize,
  color,
  heightScale,
  widthScaleSub,
  widthScale,
  width,
  height,
  square,
} from 'globalStyle';

export default styles = StyleSheet.create({
  spinnerStyle: {
    position: 'absolute',
    alignSelf: 'center',
    top: height / 2 - 30,
    bottom: height / 2 - 30,
    alignItems: 'center',
    justifyContent: 'center',
    padding: heightScale(5),
  },
  spinnerStyle2: {
    position: 'absolute',
    alignSelf: 'center',
    top: height / 2 - 30,
    bottom: height / 2 - 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  spinnerStyle3: {
    position: 'absolute',
    alignSelf: 'center',
    top: height / 2 - 30,
    bottom: height / 2 - 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  stylecomponent: {
    color: '#ffffff',
    opacity: 0.5,
    padding: 10,
  },
  backcolor: {
    backgroundColor: '#FFF',
    paddingTop: 10,
    paddingBottom: 10,
    paddingRight: 30,
    paddingLeft: 30,
    borderRadius: widthScale(5),
  },
});
