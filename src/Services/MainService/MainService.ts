import {requestSend} from './RequestSend';
import {Router, UserInfoStorage} from 'Factory';

import {generateApiError, generateNetworkError} from 'CustomError/genrateError';

const BaseUrl = {
  api: '',
};
class Service {
  async get(
    api: string,
    param: any = null,
    isLogin: boolean = false,
    contentType: number = 0,
    dataFormType: any = 0,
    baseUrlType: number = 0,
    header: any = {},
  ) {
    let url, setCofig;
    setCofig = await this.setCofig(
      api,
      header,
      baseUrlType,
      contentType,
      isLogin,
    );
    url = setCofig.url;
    if (param) {
      encodedData = await this.toFormUrlEncoded(param);
      url = setCofig.url += '?' + encodedData;
    }
    let response = await requestSend(
      setCofig.url,
      null,
      setCofig.header,
      'GET',
    );
    return response;
  }
  async post(
    api: string,
    param: any = null,
    isLogin: boolean = false,
    contentType: number = 0,
    dataFormType: any = 0,
    baseUrlType: number = 0,
    header: any = {},
  ) {
    let body, setCofig;
    setCofig = await this.setCofig(
      api,
      header,
      baseUrlType,
      contentType,
      isLogin,
    );
    if (
      (dataFormType == 0 && contentType == 0) ||
      (!dataFormType && !contentType)
    ) {
      body = await this.toFormUrlEncoded(param);
    } else if (dataFormType == 1) {
      body = await this.serializeJSON(param);
    } else if (dataFormType == 2) {
      body = JSON.stringify(param);
    }
    let response = await requestSend(
      setCofig.url,
      body,
      setCofig.header,
      'POST',
    );
    return response;
  }
  async patch(
    api: string,
    param: any = null,
    isLogin: boolean = false,
    contentType: number = 0,
    dataFormType: any = 0,
    baseUrlType: number = 0,
    header: any = {},
  ) {
    let body, setCofig;
    setCofig = await this.setCofig(
      api,
      header,
      baseUrlType,
      contentType,
      isLogin,
    );
    if (
      (dataFormType == 0 && contentType == 0) ||
      (!dataFormType && !contentType)
    ) {
      body = await this.toFormUrlEncoded(param);
    } else if (dataFormType == 1) {
      body = await this.serializeJSON(param);
    } else if (dataFormType == 2) {
      body = JSON.stringify(param);
    }
    let response = await requestSend(
      setCofig.url,
      body,
      setCofig.header,
      'PATCH',
    );
    return response;
  }
  async delete(
    api: string,
    param: any = null,
    isLogin: boolean = false,
    contentType: number = 0,
    dataFormType: any = 0,
    baseUrlType: number = 0,
    header: any = {},
  ) {
    let body, setCofig;
    setCofig = await this.setCofig(
      api,
      header,
      baseUrlType,
      contentType,
      isLogin,
    );
    if (
      (dataFormType == 0 && contentType == 0) ||
      (!dataFormType && !contentType)
    ) {
      body = await this.toFormUrlEncoded(param);
    } else if (dataFormType == 1) {
      body = await this.serializeJSON(param);
    } else if (dataFormType == 2) {
      body = JSON.stringify(param);
    }
    let response = await requestSend(
      setCofig.url,
      body,
      setCofig.header,
      'DELETE',
    );
    return response;
  }

  async put(
    api: string,
    param: any = null,
    isLogin: boolean = false,
    contentType: number = 0,
    dataFormType: any = 0,
    baseUrlType: number = 0,
    header: any = {},
  ) {
    let body, setCofig;
    setCofig = await this.setCofig(
      api,
      header,
      baseUrlType,
      contentType,
      isLogin,
    );

    if (
      (dataFormType == 0 && contentType == 0) ||
      (!dataFormType && !contentType)
    ) {
      body = await this.toFormUrlEncoded(param);
    } else if (dataFormType == 1) {
      body = await this.serializeJSON(param);
    } else if (dataFormType == 2) {
      body = JSON.stringify(param);
    }

    let response = await requestSend(
      setCofig.url,
      body,
      setCofig.header,
      'PUT',
    );
    return response;
  }

  // sets configs e.g header, contentType
  async setCofig(api, header, baseUrlType, contentType, isLogin) {
    let url = BaseUrl.api + api,
      token,
      Content_Type = 'application/x-www-form-urlencoded';
    if (contentType == 1) {
      Content_Type = 'application/json'; // default value for Content_Type
    }
    if (contentType == 2) {
      Content_Type = 'multipart/form-data';
    }
    var requestHeader = {
      Accept: 'application/json',
      'Content-Type': Content_Type,
    };
    if (header) {
      requestHeader = Object.assign(requestHeader, header);
    }
    if (baseUrlType === 1) {
      url = this.baseUrl.pyment + api;
    }
    if (baseUrlType === 2) {
      url = api;
    }

    if (isLogin) {
      let response = await UserInfoStorage.getToken();
      if (response) {
        requestHeader = Object.assign(requestHeader, {
          Authorization: 'Bearer ' + response,
        });
      } else {
        generateApiError(
          requestHeader,
          {status: 402},
          url,
          null,
          'authorization',
          null,
        );
        return;
      }
    }
    return {
      header: requestHeader,
      url: url,
    };
  }

  async toFormUrlEncoded(param) {
    let promise = new Promise(async (resolve, reject) => {
      const formBody = Object.keys(param)
        .map(
          (key) =>
            encodeURIComponent(key) + '=' + encodeURIComponent(param[key]),
        )
        .join('&');
      return resolve(formBody);
    });
    return promise;
  }
  async serializeJSON(data) {
    return Object.keys(data)
      .map(function (keyName) {
        return (
          encodeURIComponent(keyName) + '=' + encodeURIComponent(data[keyName])
        );
      })
      .join('&');
  }
}

module.exports = {Service: new Service()};
