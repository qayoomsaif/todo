import React, {Component} from 'react';
import {View, Text, Image} from 'react-native';
import styles from './splash.style';
import SplashController from './splash.controller';
import {connect} from 'react-redux';
import {changeUserInfo} from 'redux-store/actions';
class SplashComponent extends Component {
  controller = new SplashController(this);
  async componentDidMount() {
    this.controller.loadInformation();
  }
  componentWillMount() {
    console.ignoredYellowBox = ['Remote debugger'];
    console.disableYellowBox = true;
  }
  setUserInfoInRedux(respose) {
    this.props.dispatch(changeUserInfo(respose));
  }
  render() {
    return (
      <View style={styles.maincontainer}>
        <View style={styles.logoDiscContainer}>
          <Image
            style={styles.logoicon}
            source={require('src/assets/icon/youcan_white_logo.png')}
            resizeMode={'contain'}
          />
          <Text style={styles.subtext}>
            {'Best Place Maintain To Do List'}
          </Text>
        </View>
        <View style={styles.imagecontainer}>
          <Image
            style={styles.image}
            source={require('src/assets/icon/bg.png')}
          />
        </View>
      </View>
    );
  }
}

const mapStateToProps = ({account}) => {
  const {userInfo, toDoList} = account;
  return {userInfo, toDoList};
};
export default connect(mapStateToProps)(SplashComponent);
