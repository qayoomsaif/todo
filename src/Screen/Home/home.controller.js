import { Router, UserInfoStorage } from "src/Factory";
export default class HomeController {
  constructor(object) { this.object = object }
  async logOut() {
    await UserInfoStorage.userLogout()
    await this.object.setUserInfoInRedux(null)
    await this.object.upDateToDoList([])
    Router.loginComponent()
  }
}
